package com.findmybuddy.restws;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.findmybuddy.restws.exception.MyBuddyException;
import com.findmybuddy.restws.service.MyLocationService;
import com.findmybuddy.restws.utils.FindMyBuddyConstants;

/*{
 "number":"8123695220",
 "latitude":"38.898556",
 "longitude":"-77.037852 ",
 "IpAddress":"203.89.151.35",
 "mobileCountryCode":"",
 "mobileNetworkCode":"",
 "locationAreaCode":"",
 "gsmCellId":""
 }*/
@Component
@Path(FindMyBuddyConstants.PATH_MY_LOCATION)
public class LocationResource {

	@Autowired
	MyLocationService locationService;

	@POST
	@Path(FindMyBuddyConstants.PATH_UPDATE_MY_LOCATION)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveMyLocation(String location) {

		String response = null;
		try {
			response = locationService.saveMyLocation(location);
		} catch (JsonGenerationException e) {
			throw new MyBuddyException(
					"JsonGenerationException while storing my location"
							+ e.getMessage());
		} catch (JsonMappingException e) {
			throw new MyBuddyException(
					"JsonMappingException while storing my location"
							+ e.getMessage());
		} catch (IOException e) {
			throw new MyBuddyException("IOException while storing my location"
					+ e.getMessage());
		} catch (JSONException e) {
			throw new MyBuddyException(
					"JSONException while storing my location" + e.getMessage());
		}
		return getResponse(response);
	}
	
	@GET
	@Path(FindMyBuddyConstants.PATH_GET_MY_DETAILS)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMyBuddylocation(@PathParam("mobileNo") String mobileNo) {
		String response = null;
		try {
			response = locationService.getMyLocation(mobileNo);
			if (response == null) {
				throw new MyBuddyException(
						"No location has been found for the given number");
			}
		} catch (JsonGenerationException e) {
			throw new MyBuddyException(
					"JsonGenerationException while getting Friend location"
							+ e.getMessage());
		} catch (JsonMappingException e) {
			throw new MyBuddyException(
					"JsonMappingException while getting Friend location"
							+ e.getMessage());
		} catch (IOException e) {
			throw new MyBuddyException(
					"IOException while getting Friend location"
							+ e.getMessage());
		}
		return getResponse(response);

	}

	public Response getResponse(String messge) {
		return Response
				.status(200)
				.entity(messge)
				.header("Access-Control-Allow-0rigin", "*")
				.header("Access-Control-Allow-Methods",
						"API, CRUNCHIFYGET, GET, POST, PUT, UPDATE, OPTIONS")
				.build();
	}

}
