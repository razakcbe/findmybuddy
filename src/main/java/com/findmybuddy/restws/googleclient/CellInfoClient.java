package com.findmybuddy.restws.googleclient;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.findmybuddy.restws.domain.MyLocation;
import com.findmybuddy.restws.dto.MyLocationDto;

public class CellInfoClient {

	public static MyLocation getAddressFromCellId(MyLocationDto locationObj,
			MyLocation myLocation) throws JSONException, IOException {

		String url = "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyBdxdBEmlQwgddLH7FWuRYajciszppGUJo";
		JSONObject jsonObject = populateJsonObject(locationObj);
		Response res = ClientBuilder.newClient()
				.register(JacksonJsonProvider.class).target(url)
				.request(MediaType.TEXT_PLAIN).post(Entity.json(jsonObject));

		if (res.getStatus() == Response.Status.OK.getStatusCode()) {
			JSONObject response = new JSONObject(
					IOUtils.toString((InputStream) res.getEntity()));
			JSONObject object = response.getJSONObject("location");
			myLocation.setLatitude(object.getString("lat"));
			myLocation.setLongitude(object.getString("lng"));
			myLocation.setLastDetectedLocation(GoogleAddressClient
					.getAddressFromCoordinates(myLocation.getLatitude(),
							myLocation.getLongitude()));

		}

		return myLocation;
	}

	private static JSONObject populateJsonObject(MyLocationDto locationObj)
			throws JSONException {
		JSONObject jsonObject = new JSONObject();
		JSONObject jsonObject1 = new JSONObject();
		jsonObject.put("homeMobileCountryCode",
				locationObj.getMobileCountryCode());
		jsonObject.put("homeMobileNetworkCode",
				locationObj.getMobileNetworkCode());
		jsonObject.put("radioType", "gsm");
		JSONArray array = new JSONArray();
		array.put(jsonObject1);
		jsonObject1.put("homeMobileCountryCode",
				locationObj.getMobileCountryCode());
		jsonObject1.put("homeMobileNetworkCode",
				locationObj.getMobileNetworkCode());
		jsonObject1.put("cellId", locationObj.getGsmCellId());
		jsonObject1.put("locationAreaCode", locationObj.getLocationAreaCode());
		jsonObject.put("cellTowers", array);
		return jsonObject;

	}

}
