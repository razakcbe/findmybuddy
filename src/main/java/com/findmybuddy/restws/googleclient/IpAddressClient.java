package com.findmybuddy.restws.googleclient;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.common.util.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.findmybuddy.restws.domain.MyLocation;

public class IpAddressClient {

	public static MyLocation getIpAddressGeoLoacation(String ipAddress,
			MyLocation myLocation) throws JSONException, IOException {
		String googleUrl = "http://freegeoip.net/json/" + ipAddress;

		Response res = ClientBuilder.newClient()
				.register(JacksonJsonProvider.class).target(googleUrl)
				.request(MediaType.TEXT_PLAIN).get();

		JSONObject response = new JSONObject(IOUtils.toString((InputStream) res
				.getEntity()));

		return formatAddress(response, myLocation);
	}

	public static MyLocation formatAddress(JSONObject jsonObject,
			MyLocation myLocation) throws JSONException, IOException {
		String lastDetectedLocation = null;
		String zipCode = (String) jsonObject.get("zipcode");
		String latitude = String.valueOf(jsonObject.get("latitude"));
		String longitude = String.valueOf(jsonObject.get("longitude"));
		// if zipcode get lat and lang based upon zipcode
		if (!StringUtils.isEmpty(zipCode)) {
			lastDetectedLocation = GoogleAddressClient
					.getLatAndLonFromPincode(zipCode);
		}
		// if lat and lang get location
		else if (!StringUtils.isEmpty(latitude)
				&& !StringUtils.isEmpty(longitude)) {
			myLocation.setLatitude(latitude);
			myLocation.setLongitude(longitude);
			lastDetectedLocation = GoogleAddressClient
					.getAddressFromCoordinates(latitude, longitude);
		} else {
			lastDetectedLocation = (String) jsonObject.get("city")
					+ (String) jsonObject.get("region_name");
		}
		myLocation.setLastDetectedLocation(lastDetectedLocation);
		return myLocation;
	}
}
