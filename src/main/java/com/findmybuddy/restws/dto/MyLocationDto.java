/**
 * 
 */
package com.findmybuddy.restws.dto;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author Razak
 * 
 */
public class MyLocationDto {

	@JsonProperty(value = "number")
	private String number;

	@JsonProperty(value = "latitude")
	private String latitude;

	@JsonProperty(value = "longitude")
	private String longitude;

	@JsonProperty(value = "IpAddress")
	private String IpAddress;

	@JsonProperty(value = "mobileCountryCode")
	private String mobileCountryCode;

	@JsonProperty(value = "mobileNetworkCode")
	private String mobileNetworkCode;

	@JsonProperty(value = "locationAreaCode")
	private String locationAreaCode;

	@JsonProperty(value = "gsmCellId")
	private String gsmCellId;

	@JsonProperty(value = "lastDetectedTime")
	private String lastDetectedTime;

	@JsonProperty(value = "lastDetectedLocation")
	private String lastDetectedLocation;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getIpAddress() {
		return IpAddress;
	}

	public void setIpAddress(String ipAddress) {
		IpAddress = ipAddress;
	}

	public String getMobileCountryCode() {
		return mobileCountryCode;
	}

	public void setMobileCountryCode(String mobileCountryCode) {
		this.mobileCountryCode = mobileCountryCode;
	}

	public String getMobileNetworkCode() {
		return mobileNetworkCode;
	}

	public void setMobileNetworkCode(String mobileNetworkCode) {
		this.mobileNetworkCode = mobileNetworkCode;
	}

	public String getLocationAreaCode() {
		return locationAreaCode;
	}

	public void setLocationAreaCode(String locationAreaCode) {
		this.locationAreaCode = locationAreaCode;
	}

	public String getGsmCellId() {
		return gsmCellId;
	}

	public void setGsmCellId(String gsmCellId) {
		this.gsmCellId = gsmCellId;
	}

	public String getLastDetectedTime() {
		return lastDetectedTime;
	}

	public void setLastDetectedTime(String lastDetectedTime) {
		this.lastDetectedTime = lastDetectedTime;
	}

	public String getLastDetectedLocation() {
		return lastDetectedLocation;
	}

	public void setLastDetectedLocation(String lastDetectedLocation) {
		this.lastDetectedLocation = lastDetectedLocation;
	}

}
