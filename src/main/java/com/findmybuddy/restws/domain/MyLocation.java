package com.findmybuddy.restws.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MY_LOCATION")
public class MyLocation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="number")
	private MyDetails details;

	@Column(name = "latitude", length = 50)
	private String latitude;

	@Column(name = "longitude", length = 50)
	private String longitude;

	@Column(name = "lastDetectedLocation", length = 400)
	private String lastDetectedLocation;

	@Column(name = "lastDetectedTime", length = 50)
	private Date lastDetectedTime;

	@Column(name = "accuracy", length = 8)
	private String accuracy;

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLastDetectedLocation() {
		return lastDetectedLocation;
	}

	public void setLastDetectedLocation(String lastDetectedLocation) {
		this.lastDetectedLocation = lastDetectedLocation;
	}

	public Date getLastDetectedTime() {
		return lastDetectedTime;
	}

	public void setLastDetectedTime(Date lastDetectedTime) {
		this.lastDetectedTime = lastDetectedTime;
	}

	public String getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(String accuracy) {
		this.accuracy = accuracy;
	}

	public MyDetails getDetails() {
		return details;
	}

	public void setDetails(MyDetails details) {
		this.details = details;
	}
}