/**
 * 
 */
package com.findmybuddy.restws.service.impl;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;

import com.findmybuddy.restws.dao.MyLocationDao;
import com.findmybuddy.restws.domain.MyDetails;
import com.findmybuddy.restws.domain.MyLocation;
import com.findmybuddy.restws.dto.MyDetailsDto;
import com.findmybuddy.restws.dto.MyLocationDto;
import com.findmybuddy.restws.googleclient.CellInfoClient;
import com.findmybuddy.restws.googleclient.GoogleAddressClient;
import com.findmybuddy.restws.googleclient.IpAddressClient;
import com.findmybuddy.restws.helper.DataPopulationHelper;
import com.findmybuddy.restws.service.MyLocationService;

/**
 * @author Razak
 * 
 */

public class MyLocationServiceImpl implements MyLocationService {

	@Autowired
	MyLocationDao locationDaoImpl;

	@Override
	public String saveMyLocation(String location) throws JsonParseException,
			JsonMappingException, IOException, JSONException {

		String address = null;
		ObjectMapper mapper = new ObjectMapper();
		String accuracy = null;
		MyLocationDto locationObj = mapper.readValue(location, MyLocationDto.class);
		MyLocation myLocation = DataPopulationHelper
				.populateLocationFromDto(locationObj);
		if (StringUtils.isNotEmpty(locationObj.getLatitude())
				&& StringUtils.isNotEmpty(locationObj.getLongitude())) {
			address = GoogleAddressClient.getAddressFromCoordinates(locationObj.getLatitude(), locationObj.getLongitude());
			myLocation.setLastDetectedLocation(address);
			accuracy = "high";
		} else if (StringUtils.isNotEmpty(locationObj.getMobileNetworkCode())
				&& StringUtils.isNotEmpty(locationObj.getMobileCountryCode())
				&& StringUtils.isNotEmpty(locationObj.getMobileNetworkCode())
				&& StringUtils.isNotEmpty(locationObj.getLocationAreaCode())) {
			myLocation = getCellInfoGeoLoacation(locationObj, myLocation);
			accuracy = "medium";
		} else {
			myLocation = getIpAddressGeoLoacation(locationObj.getIpAddress(),myLocation);
			accuracy = "low";
		}
		myLocation.setAccuracy(accuracy);
		locationDaoImpl.saveMyLocation(myLocation,locationObj.getNumber());
		return locationObj.toString();
	}

	@Override
	public void saveMyDetails(String myDetails) throws JsonParseException,
			JsonMappingException, IOException {

		MyDetailsDto detailsDto = null;
		ObjectMapper mapper = new ObjectMapper();

		detailsDto = mapper.readValue(myDetails, MyDetailsDto.class);
		locationDaoImpl.saveMyDetails(DataPopulationHelper
				.populateDetailsFromDto(detailsDto));

	}

	@Override
	public String getMyLocation(String number) throws JsonGenerationException,
			JsonMappingException, IOException {
		String jsonLocation = null;
		ObjectMapper mapper = new ObjectMapper();
		MyLocation location = locationDaoImpl.getMyLocation(number);
		if (location != null) {
			MyLocationDto locationDto = DataPopulationHelper
					.populateToDto(location);
			jsonLocation = mapper.writeValueAsString(locationDto);
		}
		return jsonLocation;
	}

	public MyLocation getIpAddressGeoLoacation(String ipAddress,
			MyLocation myLocation) throws JSONException, IOException {
		myLocation = IpAddressClient.getIpAddressGeoLoacation(ipAddress,
				myLocation);
		return myLocation;
	}

	public MyLocation getCellInfoGeoLoacation(MyLocationDto locationObj,
			MyLocation myLocation) throws JSONException, IOException {
		myLocation = CellInfoClient.getAddressFromCellId(locationObj,
				myLocation);
		return myLocation;
	}

}
